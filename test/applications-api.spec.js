import { ApplicationsAPI } from '../src/applications-api'
import {
    isPromise,
    isApplication,
    isPaginationInfo,
    implementsPaginated,
} from './helpers'

describe('ApplicationsAPI', () => {
    console.log(Array.isArray)
    let api = null
    beforeAll(() => api = new ApplicationsAPI())

    describe('#getAll', () => {
        it('is a function', () => {
            expect(typeof api.getAll).toBe('function')
        })
        it('returns a promise', () => {
            expect(isPromise(api.getAll())).toBe(true)
        })
        it('resolves into an array of applications', async (done) => {
            try {
                let applications = await api.getAll()
                expect(Array.isArray(applications)).toBe(true)
                expect(applications.every(isApplication)).toBe(true)
                done()
            } catch(e) {
                done.fail(e)
            }
        })
        it('resolved array implements Paginated')
        it('with limit=X, returns an array no longer than X')
        // TODO: Add more tests here... :)
    })
    describe('#get', () => {
        it('is a function', () => expect(typeof api.get).toBe('function'))
        it('throws if no id provided', async (done) => {
            try {
                let applications = await api.get()
                done.fail('expected a thrown error, did not throw')
            } catch(e) {
                expect(e instanceof TypeError).toBe(true)
                expect(e.message).toMatch(/id/)
                done()
            }
        })
        it('throws if provided id is not a valid number')
        it('returns a promise when a valid id is provided', () => {
            expect(isPromise(api.get(1337))).toBe(true)
        })
        it('resolves into a valid application when a valid id is provided')
        it('resolves into null when a non existing id is provided')
        // TODO: Add more tests here... :)
    })
    describe('#delete', () => {
        it('is a function', () => expect(typeof api.delete).toBe('function'))
        it('returns a promise')
        it('rejects when non existing id is provided', (done) => {
            api.delete(1337)
            .then(() => fail('did not reject'))
            .catch((e) => expect(error).toBeDefined())
            .then(() => done())
        })
        // TODO: Add more tests here... :)
    })
})