## Prerequisites
- Node 6 or greater
- npm v3 or greater
- Google Chrome browser

## How this works?
- Clone this repo
- Below is the **[problem](#markdown-header-problem)**. Read it from top to bottom and apply a solution of your choice.
- Review your code... and review it again. Try to pay special attention to details.
- Make a [patch](http://www.thegeekstuff.com/2014/03/git-patch-create-and-apply) with your changes and send it to us with a clear description of what you did, why and any extra comments (if any)
- Wait for our response :)

Thanks for taking the time to take this little test with us.

## What we will rate
When reviewing your solution we will mainly look into this:

- The module developed actually works
- Correctly applying [SOLID](https://es.wikipedia.org/wiki/SOLID)
- Quality of the code written: codestyle, good programming practices...
- Quality of the dependency tree (if any)
- How good your tests are?: Did you identify any borderline scenario? Did you test it properly?
- Writing propper documentation

## Batteries included
This project includes some preconfigured tools for you to only worry about coding right away:

- [Babel](https://babeljs.io/) (with [babel-preset-env](http://babeljs.io/docs/plugins/preset-env/)): Allows you to use the latest EcmaScript standard features and functions without worrying if the browser where you will be running your code supports it.
- [Webpack](https://webpack.js.org/): Allows you to build web applications with arbitrary module dependency by using `require`  and `import`.
- [Karma](http://karma-runner.github.io/1.0/index.html): Test engine that allows to run tests on actual browsers. It's already configured for using Webpack + Babel and watch for file changes under `src/`.
- [Jasmine](https://jasmine.github.io/index.html): Testing and assertion library.
- [ESDoc](https://esdoc.org/): Pretty code documentation generator for JavaScript/EcmaScript.

After cloning the repo just do as follows:
```bash
$ npm install
$ npm run docs # For building and opening the docs
$ npm run build # For a one-time-build via Webpack
$ npm test # For running the tests in watch mode
$ npm test -- --single-run # For running the tests once
```

## Problem

### What we want?
We are in the process of moving our internal CRM to a new stack and change its current implementation into a more SPA oriented web application. In order to start moving things forward we are starting with the frontend API consumer library. Since we are only focusing on the API consumption, this library makes use of no extra framework (no angular, no react...).

Someone on our team was kindly enough to define the interface which the main library class [ApplicationsAPI](src/applications-api/i-applications-api.js) will implement. What a nice guy! Nevertheless, he asked for two things from **you**:

- To write the tests that assert that the spec on the implementation is correct: some are already implemented, some are outlined, the remaining should be outlined and implemented by you.
- To write a mock api that follows the said interface for others to use while he finishes writing the actual API consumer. This mock must run only on the browser and have no external dependencies.

### How are you going to do it
You can choose where to start: spec tests or mock implementation (we are not implicitly requesting you to do [TDD](https://en.wikipedia.org/wiki/Test-driven_development), just do what you feel more comfortable with). Anyways, you will end up working with both, since the project is configured for running tests agains the mock implemtantion for now (remember, the actual one is not yet developed :()

Optionally, you are also allowed to install any packages that help you to write better code (linting, static type checking...) if you explain why and how they work.

Finally, feel free to modify and peek on any other file from the project. We think you will only need to work on `src/applications-api/mock-applications-api.js` and `test/applications-api.spec.js`, but if you make changes outside those, please justify why when sending your solution.
