'use strict'
const CleanPlugin = require('clean-webpack-plugin')
const path = require('path')

module.exports = {
    entry: {
        'vendor': './src/vendor.js',
        'applications-api': './src/applications-api',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                // Disable AMD on every dependency
                include: /node_modules/,
                loader: 'imports-loader?define.amd=>false'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader?+cacheDirectory'
            },
        ]
    },
    plugins: [
        new CleanPlugin([ 'dist' ]),
    ],
}
