/**
 * Always associated with an array. Represents where the array "resides" among
 * a larger collection.
 *
 * @typedef {Object} PaginationInfo
 * @property {!number} start The index of the first element of the array among
 *                          the larger collection.
 * @property {!number} limit The amount of elements the array contains.
 * @property {!number} total The total amount of elements available at the
 *                          larger collection.
 */

/**
 * @typedef {Object} Application
 * @property {number} id - The unique identifier of the application
 * @property {string} name - The name of the application, which will never be empty
 */

/**
 * A chunk of a larger collection. Contains info about the current chunk
 * and the whole collection it belongs to.
 *
 * Note that when requesting {@link Paginated} resources its length might not
 * be the one specified by `limit` on the request params. If the request
 * pagination parameters exceed the total collection lenght the chunk might be
 * shorter than `limit` specified or even empty.
 *
 * @interface
 */
export class Paginated {
    /** @type {Paginationinfo} */
    get pagination() {}
}

/**
 * @interface
 */
export class IApplicationsAPI {
    /**
     * Initializes a new applications api consumer.
     */
    constructor() {}

    /**
     * Retrieves a chunk of the complete collection of applications available.
     * The returned array (after promise is resolved) implements {@link Paginated}.
     *
     * @param {!Object} opt
     * @param {!number} [opt.start=10] - The index among the whole applications
     *                                collection of the first element available
     *                                on the retunred array.
     * @param {!number} [opt.limit=10] - The max amount of elements contained on
     *                                the retuned array.
     * @return {Promise<Application[]>}
     */
    getAll({ start = 0, limit = 10 }) {}

    /**
     * Retrieves the application with the provided id or null if not found.
     * @param {!number} id - The id of the application to retrieve.
     * @return {Promise<?Application>}
     * @throws {TypeError} If provided id is not a valid number.
     */
    get(id) {}

    /**
     * Deletes the application with the provided id. The resulting promise will
     * be rejected if no application matches the given id.
     * @param {!number} id - The id of the application to delete.
     * @return {Promise<null,Error>}
     * @throws {TypeError} If provided id is not a valid number.
     */
    delete(id) {}
}
